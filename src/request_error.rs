use std::fmt;

#[derive(Debug)]
pub struct RequestError {
    details: String,
}

impl RequestError {}

impl fmt::Display for RequestError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RequestError: {}", self.details)
    }
}

impl From<reqwest::Error> for RequestError {
    fn from(_value: reqwest::Error) -> Self {
        RequestError {
            details: String::from("reqwest::Error"),
        }
    }
}

impl From<reqwest::header::ToStrError> for RequestError {
    fn from(_value: reqwest::header::ToStrError) -> Self {
        RequestError {
            details: String::from("reqwest::header::ToStrError"),
        }
    }
}

impl From<serde_json::Error> for RequestError {
    fn from(value: serde_json::Error) -> Self {
        RequestError {
            details: format!("serde_json::Error {}", value),
        }
    }
}
