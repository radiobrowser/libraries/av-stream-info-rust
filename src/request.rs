use crate::request_error::RequestError;
use futures_util::StreamExt;
use std::{collections::HashMap, str::Utf8Error};

pub struct HttpHeaders {
    pub code: u32,
    pub headers: HashMap<String, String>,
}

pub struct Request {
    pub info: HttpHeaders,
    content_vec: Vec<u8>,
    content_length: Option<u64>,
    has_http: bool,
    has_https: bool,
}

impl Request {
    pub async fn new(url_str: &str, agent: &str, timeout: u32) -> Result<Request, RequestError> {
        let client = reqwest::Client::builder()
            .user_agent(agent)
            .timeout(std::time::Duration::from_secs(timeout as u64))
            .build()?;

        let response = client.get(url_str).send().await?;

        let status = response.status();
        let content_length = response.content_length();
        let code = status.as_u16() as u32;

        let mut headers_map = HashMap::new();
        for (key, value) in response.headers() {
            headers_map.insert(key.to_string(), value.to_str()?.to_string());
        }

        let mut body_stream = response.bytes_stream();
        let mut content_vec = Vec::new();
        let mut total_read = 0;
        let max_bytes: usize = content_length.unwrap_or(4096) as usize;

        while let Some(chunk_result) = body_stream.next().await {
            let chunk = chunk_result?;
            let remain = max_bytes.saturating_sub(total_read);

            if remain == 0 {
                break;
            }
            let to_take = chunk.len().min(remain);
            content_vec.extend_from_slice(&chunk[..to_take]);
            total_read += to_take;
        }

        let http_headers = HttpHeaders {
            code,
            headers: headers_map,
        };

        let mut has_http = url_str.to_lowercase().starts_with("http://");
        let mut has_https = url_str.to_lowercase().starts_with("https://");

        // check http/https separately
        {
            let other_url = if has_http {
                url_str.to_lowercase().replace("http://", "https://")
            } else {
                url_str.to_lowercase().replace("https://", "http://")
            };

            let response = client.get(other_url).send().await?;
            let other_status = response.status();
            let other_content_length = response.content_length();
            let other_code = other_status.as_u16() as u32;

            // if the second request is also ok then mark has_xxxx as ok
            if code == other_code && content_length == other_content_length {
                if !has_http {
                    has_http = true;
                } else {
                    has_https = true;
                }
            }
        };

        let req = Request {
            info: http_headers,
            content_vec,
            content_length,
            has_http,
            has_https,
        };

        Ok(req)
    }

    pub fn text(&self) -> Result<&str, Utf8Error> {
        std::str::from_utf8(&self.content_vec)
    }

    pub fn had_ssl_error(&self) -> bool {
        false
    }

    pub fn content_length(&self) -> Option<u64> {
        self.content_length
    }

    pub fn has_http(&self) -> bool {
        self.has_http
    }

    pub fn has_https(&self) -> bool {
        self.has_https
    }
}
