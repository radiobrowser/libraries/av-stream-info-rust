//! This library can analyze a http/https address and check if leads to an audio or a video stream
//! If so, then it will extract information about the stream from its metadata or in case of HLS streams
//! from its master playlist file.
//!
//! # Example
//! ```ignore
//! let list = av_stream_info_rust::check_tree("https://example.com/test.m3u", 10, 3, 3);
//! for item in list {
//!     println!("{:?}", item);
//! }
//! ```

extern crate hls_m3u8;
#[macro_use]
extern crate log;
extern crate native_tls;
extern crate playlist_decoder;
extern crate reqwest;
extern crate url;

extern crate serde;
extern crate serde_json;

mod decodeerror;
mod lat_long;
mod request;
mod request_error;
mod streamcheck;
mod streamcheckerror;
mod streamcheckresult;
mod streaminfo;

mod http_config;

use std::thread;
use std::time::Duration;

pub use decodeerror::DecodeError;
pub use http_config::extract_from_homepage_async;
#[cfg(feature = "blocking")]
pub use http_config::extract_from_homepage;
pub use http_config::MetaInfoFile;
pub use lat_long::LatLong;
pub use streamcheckerror::StreamCheckError;
pub use streamcheckresult::StreamCheckResult;
pub use streamcheckresult::UrlType;
pub use streaminfo::StreamInfo;

/// Check url for audio/video stream.
/// # Example
/// ```ignore
/// let item = av_stream_info_rust::check_tree("https://example.com/test.m3u", 10, 3, 3);
/// println!("{:#?}", item);
/// ```
/// # Arguments
/// * `url` - The url to check
/// * `timeout` - TCP timeout for connect and read in seconds
/// * `max_depth` - How many layers of http redirects or playlists should be followed
/// * `retries` - Retry how many times to find at least one working stream
/// * `early_exit_on_first_ok` - return from checking as early as 1 working stream has been found
#[cfg(feature = "blocking")]
pub fn check_tree(
    url: &str,
    timeout: u32,
    max_depth: u8,
    max_retries: u8,
) -> StreamCheckResult {
    use async_std::task;
    let mut retries: u8 = 0;
    loop {
        let mut urllist = vec![url.to_string()];
        debug!("Check retry {}", retries);
        for depth in 1..max_depth {
            debug!("Check depth {}", depth);
            for url in urllist.clone() {
                debug!("Check url '{}'", url);
                let result = task::block_on(async {
                    streamcheck::check(&url, timeout).await
                });
                if has_ok_result(&result) {
                    return result;
                }
                match &result.info {
                    Ok(info) => match info {
                        UrlType::Playlist(list) => {
                            urllist = list.clone();
                        }
                        _ => {}
                    },
                    _ => {}
                };
            }
        }
        retries += 1;
        if retries > max_retries {
            break;
        }
        thread::sleep(Duration::from_secs(1));
    }
    return StreamCheckResult::new(url, Err(StreamCheckError::NoResult()));
}

/// Check url for audio/video stream.
/// # Example
/// ```ignore
/// let item = av_stream_info_rust::check_tree_async("https://example.com/test.m3u", 10, 3, 3).await?;
/// println!("{:#?}", item);
/// ```
/// # Arguments
/// * `url` - The url to check
/// * `timeout` - TCP timeout for connect and read in seconds
/// * `max_depth` - How many layers of http redirects or playlists should be followed
/// * `retries` - Retry how many times to find at least one working stream
/// * `early_exit_on_first_ok` - return from checking as early as 1 working stream has been found
pub async fn check_tree_async(
    url: &str,
    timeout: u32,
    max_depth: u8,
    max_retries: u8,
) -> StreamCheckResult {
    let mut retries: u8 = 0;
    loop {
        let mut urllist = vec![url.to_string()];
        debug!("Check retry {}", retries);
        for depth in 1..max_depth {
            debug!("Check depth {}", depth);
            for url in urllist.clone() {
                debug!("Check url '{}'", url);
                let result = streamcheck::check(&url, timeout).await;
                if has_ok_result(&result) {
                    return result;
                }
                match &result.info {
                    Ok(info) => match info {
                        UrlType::Playlist(list) => {
                            urllist = list.clone();
                        }
                        _ => {}
                    },
                    _ => {}
                };
            }
        }
        retries += 1;
        if retries > max_retries {
            break;
        }
        thread::sleep(Duration::from_secs(1));
    }
    return StreamCheckResult::new(url, Err(StreamCheckError::NoResult()));
}

fn has_ok_result(result: &StreamCheckResult) -> bool {
    match &result.info {
        Ok(info) => match info {
            UrlType::Stream(_) => true,
            UrlType::Playlist(_) => false,
        },
        Err(_) => false,
    }
}
